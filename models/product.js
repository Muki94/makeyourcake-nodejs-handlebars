const Sequelize = require('sequelize');
const sequelize = require('./../config/config');

module.exports.product = sequelize.define('product', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.TEXT
    },
    price: {
        type: Sequelize.DECIMAL
    },
    image: {
        type: Sequelize.STRING
    },
    cakeType: {
        type: Sequelize.STRING
    },
    cakeColor: {
        type: Sequelize.STRING
    }
});