var express = require('express');
var router = express();
const sequelize = require('./../config/config');

router.get('/', function (req, res, next) {
    sequelize
        .authenticate()
        .then(() => {
            res.send('Connection has been established successfully.');
        })
        .catch(err => {
            res.send('Unable to connect to the database:' + err.parent.code);
        });
});

module.exports = router;