var express = require('express');
var router = express.Router();

var productModel = require('./../models/product').product;

router.get("/", function (req, res, next) {
    //dohvatiti sve proizvode i prikazati ih
    productModel.findAll().then(result => {
        res.render('products.hbs', {
            products: result
        });
    });
});

router.get("/:id", function (req, res, next) {
    //dohvati jedan proizvod
    productModel.findById(req.params.id).then(result => {
        res.render('products-details.hbs', {
            product: result
        });
    });
});

module.exports = router;